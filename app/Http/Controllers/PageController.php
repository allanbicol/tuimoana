<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class PageController extends Controller
{
    function index(){
        return view('index');
    }

    function about(){
        return view('about');
    }


    function sendEmail(Request $request)
    {
        $data = [
            // 'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            // 'subject'=>$request->input('subject'),
            // 'message'=>$request->input('message'),
        ];
        // dump($data);
        // print_r($data['services']);
        // return view('front.mail.contact',['data'=>$data]);
        // exit();

    //    Mail::to('bicolallan@gmail.com')->send(new SendMail($data));

        $email = Mail::send('mail.email', ['data'=>$data], function($message) {
            $message->to('bicolallan@gmail.com');
            $message->from('tui@tuimoana.com',"Tui Moana");
            $message->subject('New subscriber from Tui\'s website.');
        });

        return back()->with('success','Message sent successfully!');
    }
}
