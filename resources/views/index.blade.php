<!DOCTYPE html>
<html lang="en-US" class="no-js">

	<head>

		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- Title -->
		<title>Tui Moana</title>

		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico">

		<!-- Font awesome -->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/layout/plugins/fontawesome/css/fontawesome-all.min.css')}}" />

		<!-- Google web fonts -->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700,700italic">

		<!-- Stylesheet -->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/layout/style.css')}}">
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/layout/media.css')}}">

		<!-- Color schema -->
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/layout/colors/blue.css')}}" class="colors">

		<!-- YTPlayer -->
		{{-- <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/layout/plugins/ytplayer/jquery.mb.ytplayer.min.css')}}"> --}}

		<!-- Settings (Remove it on your site) -->
		<!-- <link rel="stylesheet" type="text/css" href="layout/plugins/settings/settings.css"> -->

		<!-- Modernizr -->
    	<script src="{{ URL::asset('assets/layout/plugins/modernizr/modernizr.custom.js')}}"></script>

    	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    	<!--[if lt IE 9]>
      		<script src="layout/plugins/html5shiv/html5shiv.js"></script>
      		<script src="layout/plugins/respond/respond.min.js"></script>
      	<![endif]-->

	</head>

	<body>
		<div class="wrap" id="bg-image">

			<!-- Main -->
			<div id="main">
				<div class="inner">

					<!-- Header -->
					<header>
						<h1 class="logo fade-in">
							<img src="{{ URL::asset('assets/images/tui-logo.png')}}" width="200" height="200" alt="Selene" />
						</h1>
					</header>

					<!-- Content -->
					<section class="content">

						<h1 class="title">
							WELCOME TO YOUR HOME <br>AWAY FROM HOME
						</h1>

						<p class="slogan">
							We are currently under contruction, both online and on the ground - but we are so excited <br>to share our piece of paradise with you.
						</p>
                        <div style="height:50px;"></div>
						<!-- Countdown timer -->
						<!-- <div id="timer"></div> -->

						<p class="subtitle">PLEASE SUBSCRIBE SO WE CAN KEEP YOU UP
                            <br>TO DATE WITH OUR PROGRESS</p>

						<!-- Newsletter form -->
						<div id="newsletter" class="form-wrap">

                        <form action="{{route('send-mail')}}" method="post" id="newsletter-form">
                            {{ csrf_field() }}
								<p class="form-field">
									<input type="text" name="email" id="email" value="" placeholder="Your email" />
								</p>

								<p class="form-submit">
									<input type="submit" name="submit" id="submit" value="Subscribe" />
								</p>

							</form>

						</div>

						<!-- Social links -->
						<div class="social">
							<ul>

								<li>
									<a href="#" title="Twitter">
										<i class="fab fa-twitter"></i>
									</a>
								</li>

								<li>
									<a href="#" title="Facebook">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>



							</ul>
						</div>

					</section>

					<!-- Modal page toggle -->
					<div class="modal-toggle">
						<a href="{{route('aboutpage')}}" id="modal-open1" title="More Info">
							<i class="fas fa-angle-right"></i>
						</a>
					</div>

				</div>
			</div>

			<!-- Modal page: About Us -->
			<div id="modal">
				<div class="inner">

					<!-- Modal toggle -->
					<div class="modal-toggle">
						<a href="#" id="modal-close" title="Close">
							<i class="fas fa-times"></i>
						</a>
					</div>

					<!-- Content -->
					<section class="content">

						<h1 class="title">About <span>Tui Moana</span></h1>

						<!-- Columns -->
						<div class="row">

							<div class="one-half">
								<p>Tui Moana is a dream realised, of beautiful spaces in a tranquil location - founded on love.</p>
								<!-- <p>Nulla facilisi. Pellentesque ac finibus diam. Suspendisse potenti. Nullam id nibh at velit placerat ornare non non lorem.</p>
								<p>Morbi vulputate sed turpis sed mollis. Ut eget lectus eros. Donec mollis et orci eu vulputate.</p> -->
							</div>

							<div class="one-half">
								<h2><i class="fas fa-user"></i>Tui Harwood</h2>
								<p>
									Phone: +64 275 899 822<br />
									Email: tui@tuimoana.com
								</p>

								<!-- <h2><i class="fas fa-envelope"></i> Email</h2>
								<p>
									<a href="/cdn-cgi/l/email-protection#f6939b979f9ab6859f829398979b93d895999b"><span class="__cf_email__" data-cfemail="3d58505c54517d4e544958535c5058135e5250">[email&#160;protected]</span></a>
								</p> -->

								<h2><i class="fas fa-map"></i> Address</h2>
								<p>
									1030B State Highway 2<br>
									Tanners Point 3177<br>
									New Zealand
								</p>
							</div>

						</div>

					</section>

				</div>
			</div>

		</div>

		<!-- Background overlay -->
		<div class="body-bg"></div>

		<!-- Loader -->
		<div class="page-loader">
			<div class="progress">Loading...</div>
		</div>

		<!-- jQuery -->
        {{-- <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> --}}
        <script src="{{ URL::asset('assets/layout/plugins/jquery/jquery.js')}}"></script>

		<!-- Plugins -->
		<script src="{{ URL::asset('assets/layout/plugins/backstretch/jquery.backstretch.min.js')}}"></script>
		<script src="{{ URL::asset('assets/layout/plugins/plugin/jquery.plugin.min.js')}}"></script>
		<script src="{{ URL::asset('assets/layout/plugins/countdown/jquery.countdown.min.js')}}"></script>
		<script src="{{ URL::asset('assets/layout/plugins/validate/jquery.validate.min.js')}}"></script>
		<script src="{{ URL::asset('assets/layout/plugins/placeholder/jquery.placeholder.min.js')}}"></script>
		{{-- <script src="{{ URL::asset('assets/layout/plugins/ytplayer/jquery.mb.ytplayer.min.js')}}"></script> --}}

		<!-- Main -->
		<script src="{{ URL::asset('assets/layout/js/main.js')}}"></script>

		<!-- Settings (Remove it on your site) -->
		<script src="{{ URL::asset('assets/layout/plugins/settings/jquery.cookies.min.js')}}"></script>
		<script src="{{ URL::asset('assets/layout/plugins/settings/settings.js')}}"></script>

	</body>

</html>
